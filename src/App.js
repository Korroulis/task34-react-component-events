import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Dashboard from './components/containers/Dashboard';
import Login from './components/containers/Login';
import Register from './components/containers/Register';
import NotFound from './components/containers/NotFound';


function App() {

    return (
      <Router>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav-lg">
                <Link to="/">Home</Link>
                <Link to="/login">Login</Link>
                <Link to="/register">Register</Link>
              </div>
            </div>
        </nav>
        <div className="App" >
          <main>
            <Switch>
                <Route path="/" component={Login} exact /> 
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="*" component={NotFound} />
            </Switch>
          </main>

        </div>
      </Router>
      
    );

}


export default App;
