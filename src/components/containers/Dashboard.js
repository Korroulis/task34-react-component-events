import React from 'react';
import DashboardMessage from '../messages/DashboardMessage';

const Dashboard = () => {
    return (
        <div>
            <DashboardMessage />
        </div>
    )
};

export default Dashboard;